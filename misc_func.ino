float mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void printVoces() {
  for (uint8_t i=0; i<NUM_VOCES; i++) {
    Serial.print("["); 
    if (voz[i].nota==0) {
      Serial.print("-");
    } else {    
      Serial.print(voz[i].nota);
    }
    Serial.print("]");
  }
  Serial.println();
}
