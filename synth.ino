void tocarNota(uint8_t ch, uint8_t n, uint8_t v, uint8_t vel) {
  float amp = mapf(vel,0,127,0,1.0)*canal[ch].V;
  AudioNoInterrupts();
  oscs[v]->begin(amp,freq12tET[n] * canal[ch].bendFactor ,waveType[canal[ch].waveform]);
  oscs_sec[v]->begin(amp,freq12tET[n+detune_octaves[canal[ch].detune_octave]] * canal[ch].detune,waveType[canal[ch].waveform]);
  envs[v]->noteOn();
  AudioInterrupts();
  Serial.print("Note ON Ch "); Serial.print(ch); Serial.print("N "); Serial.print(n); Serial.print(" vel "); Serial.print(vel);
  Serial.print(" amp "); Serial.print(amp); Serial.print(" "); Serial.print(freq12tET[n]); Serial.println("hz");
}

void pararNota(uint8_t ch, uint8_t n, uint8_t v) {
  envs[v]->noteOff();
}

void cambiarVoces(uint8_t c) {
  for (uint8_t i=0 ; i<NUM_VOCES; i++) {
    if (voz[i].nota != 0) {
      oscs[i]->frequency(freq12tET[voz[i].nota] * canal[c].bendFactor);
      oscs_sec[i]->frequency(freq12tET[i]+detune_octaves[canal[c].detune_octave] * canal[c].bendFactor * canal[c].detune);
    }
  }
}
