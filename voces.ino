uint8_t  elegirVoz() {
  uint8_t v = 0;
  // Revisamos si alguna voz está libre y elejimos la de menor ID de todas ellas. 
  for (uint8_t n=0; n<NUM_VOCES; n++) { // 
    if (voz[n].nota==0) {
      return n;
    }
  }
  uint32_t t = voz[0].t0;
  for (uint8_t n=1; n<NUM_VOCES; n++) {
    if (voz[n].t0 < t) {
      v=n;
    }
  }
  return v;
}

uint8_t buscarVozNota(uint8_t n) {
  for (uint8_t v=0; v<NUM_VOCES;v++) {
    if (voz[v].nota==n) {
      return v;
    }
  }
  return NUM_VOCES+1;
}

