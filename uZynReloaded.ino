
#include <USBHost_t36.h>
#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include "freqs.h"
#include <EEPROM.h>

USBHost myusb;
USBHub hub1(myusb);
MIDIDevice midi1(myusb);

// Las personalizaciones

const byte  CC_ATTACK        = 73;
const byte  CC_DECAY         = 3;
const byte  CC_SUSTAIN       = 9;
const byte  CC_RELEASE       = 72;
const byte  CC_VOLUMEN       = 7;
const byte  CC_WAVEFORM      = 14;
const byte  CC_DETUNE_LEVEL  = 94;
const byte  CC_DETUNE_OCTAVE = 102;
const byte  CC_ALL_NOTE_OFF  = 123;
const byte  CC_FILTER_FREQ   = 74;
const byte  CC_FILTER_RES    = 71;
const byte  CC_FILTER_MODE    = 85;


const uint16_t ATTACK_TIME_MIN = 0;
const uint16_t ATTACK_TIME_MAX = 3000;

const uint16_t DECAY_TIME_MIN  = 0;
const uint16_t DECAY_TIME_MAX  = 3000;

const uint16_t SUSTAIN_LEV_MIN = 0.0;
const uint16_t SUSTAIN_LEV_MAX = 1.0;

const uint16_t RELEASE_TIME_MIN = 0;
const uint16_t RELEASE_TIME_MAX = 5000;

const float    VOLUMEN_MIN = 0.0;
const float    VOLUMEN_MAX = 1.0;

const float DETUNE_LEVEL_MIN = 0.0;
const float DETUNE_LEVEL_MAX = 1.0;

// No more configuration passed this line. You are entering in the wild zone!
// Spanglish mixed cosas, be careful amigo. 


// Da soundsystema


// GUItool: begin automatically generated code
AudioSynthWaveform       osc_B_4; //xy=120.99993896484375,779.0000305175781
AudioSynthWaveform       osc_D_8; //xy=117.33319091796875,1847.083251953125
AudioSynthWaveform       osc_A_2; //xy=123.6666259765625,322.4166564941406
AudioSynthWaveform       osc_C_1; //xy=124.4166259765625,205.41665649414062
AudioSynthWaveform       osc_A_8; //xy=118.33319091796875,1734.333251953125
AudioSynthWaveform       osc_C_8; //xy=118.33319091796875,1810.083251953125
AudioSynthWaveform       osc_A_1;           //xy=125.4166259765625,124.66665649414062
AudioSynthWaveform       osc_B_1; //xy=125.4166259765625,164.66665649414062
AudioSynthWaveform       osc_C_2; //xy=124.6666259765625,409.1666564941406
AudioSynthWaveform       osc_B_8; //xy=119.33319091796875,1772.333251953125
AudioSynthWaveform       osc_B_2; //xy=125.6666259765625,364.4166564941406
AudioSynthWaveform       osc_D_1; //xy=126.4166259765625,241.41665649414062
AudioSynthWaveform       osc_A_4; //xy=124.99993896484375,734.0000305175781
AudioSynthWaveform       osc_C_4; //xy=124.9998779296875,817.7500305175781
AudioSynthWaveform       osc_D_3; //xy=125.6666259765625,663.7500305175781
AudioSynthWaveform       osc_D_4; //xy=125,858.7500305175781
AudioSynthWaveform       osc_C_5; //xy=124.99989318847656,1150.083251953125
AudioSynthWaveform       osc_D_7; //xy=123.33319091796875,1642.083251953125
AudioSynthWaveform       osc_A_7; //xy=124.33319091796875,1524.333251953125
AudioSynthWaveform       osc_C_7; //xy=124.33319091796875,1602.083251953125
AudioSynthWaveform       osc_D_5; //xy=125.99995422363281,1196.083251953125
AudioSynthWaveform       osc_A_5; //xy=126.99995422363281,1065.333251953125
AudioSynthWaveform       osc_B_5; //xy=126.99989318847656,1109.333251953125
AudioSynthWaveform       osc_D_2; //xy=129.6666259765625,454.1666564941406
AudioSynthWaveform       osc_B_7; //xy=125.33319091796875,1564.333251953125
AudioSynthWaveform       osc_C_3; //xy=129.6666259765625,623.7500305175781
AudioSynthWaveform       osc_B_6; //xy=127.333251953125,1348.333251953125
AudioSynthWaveform       osc_C_6; //xy=127.333251953125,1391.083251953125
AudioSynthWaveform       osc_B_3; //xy=130.6666259765625,579.0000305175781
AudioSynthWaveform       osc_A_6; //xy=128.333251953125,1305.333251953125
AudioSynthWaveform       osc_A_3; //xy=131.6666259765625,540.0000305175781
AudioSynthWaveform       osc_D_6; //xy=129.333251953125,1433.083251953125
AudioMixer4              mix_osc_5; //xy=327.99993896484375,1183.083251953125
AudioMixer4              mix_osc_7; //xy=335.33319091796875,1589.083251953125
AudioMixer4              mix_osc_8; //xy=337.33319091796875,1782.083251953125
AudioMixer4              mix_osc_6; //xy=342.333251953125,1369.083251953125
AudioMixer4              mix_osc_1; //xy=357.75,185.41665649414062
AudioMixer4              mix_osc_2; //xy=366.6666259765625,376.1666564941406
AudioMixer4              mix_osc_4; //xy=367.9998779296875,786.7500305175781
AudioMixer4              mix_osc_3; //xy=370.6666259765625,594.7500305175781
AudioFilterStateVariable filter6; //xy=546.666748046875,1312.40478515625
AudioFilterStateVariable filter8; //xy=551,1737.0714111328125
AudioFilterStateVariable filter5; //xy=563.25,1132.6666259765625
AudioFilterStateVariable filter7; //xy=574.583251953125,1536.333251953125
AudioFilterStateVariable filter1;        //xy=582.2232666015625,129.08039093017578
AudioFilterStateVariable filter4; //xy=585.916748046875,754.2381591796875
AudioFilterStateVariable filter2; //xy=588.9256591796875,345.5328369140625
AudioFilterStateVariable filter3; //xy=591.5,536.5
AudioMixer4              mix_filter_5; //xy=771.669677734375,1167.3719482421875
AudioMixer4              mix_filter_6; //xy=771.08642578125,1346.110107421875
AudioMixer4              mix_filter_1;         //xy=775.9285888671875,166.49998474121094
AudioMixer4              mix_filter_2; //xy=790.3453369140625,355.23809814453125
AudioMixer4              mix_filter_8; //xy=785.419677734375,1759.776611328125
AudioMixer4              mix_filter_7; //xy=786.3363037109375,1573.03857421875
AudioMixer4              mix_filter_3; //xy=791.919677734375,576.205322265625
AudioMixer4              mix_filter_4; //xy=807.33642578125,767.9434204101562
AudioEffectEnvelope      env1;           //xy=961.41650390625,168.7618408203125
AudioEffectEnvelope      env3;           //xy=961.0836791992188,575
AudioEffectEnvelope      env4;           //xy=961.0835571289062,768.0001220703125
AudioEffectEnvelope      env2;           //xy=964.7503662109375,356.3333740234375
AudioEffectEnvelope      env8;           //xy=961.0831298828125,1760.6668701171875
AudioEffectEnvelope      env7;           //xy=968.0831298828125,1603.000244140625
AudioEffectEnvelope      env6;           //xy=994.41650390625,1344.6668701171875
AudioEffectEnvelope      env5;           //xy=996.083251953125,1165.0001220703125
AudioMixer4              mix_in_B;       //xy=1375.4166259765625,1474.666748046875
AudioMixer4              mix_in_A;       //xy=1400.32177734375,446.1429443359375
AudioMixer4              mix_out;        //xy=1753.4169921875,1040.3333129882812
AudioOutputI2S           i2s1;           //xy=1958.75048828125,1037.3333129882812
AudioConnection          patchCord1(osc_B_4, 0, mix_osc_4, 1);
AudioConnection          patchCord2(osc_D_8, 0, mix_osc_8, 3);
AudioConnection          patchCord3(osc_A_2, 0, mix_osc_2, 0);
AudioConnection          patchCord4(osc_C_1, 0, mix_osc_1, 2);
AudioConnection          patchCord5(osc_A_8, 0, mix_osc_8, 0);
AudioConnection          patchCord6(osc_C_8, 0, mix_osc_8, 2);
AudioConnection          patchCord7(osc_A_1, 0, mix_osc_1, 0);
AudioConnection          patchCord8(osc_B_1, 0, mix_osc_1, 1);
AudioConnection          patchCord9(osc_C_2, 0, mix_osc_2, 2);
AudioConnection          patchCord10(osc_B_8, 0, mix_osc_8, 1);
AudioConnection          patchCord11(osc_B_2, 0, mix_osc_2, 1);
AudioConnection          patchCord12(osc_D_1, 0, mix_osc_1, 3);
AudioConnection          patchCord13(osc_A_4, 0, mix_osc_4, 0);
AudioConnection          patchCord14(osc_C_4, 0, mix_osc_4, 2);
AudioConnection          patchCord15(osc_D_3, 0, mix_osc_3, 3);
AudioConnection          patchCord16(osc_D_4, 0, mix_osc_4, 3);
AudioConnection          patchCord17(osc_C_5, 0, mix_osc_5, 2);
AudioConnection          patchCord18(osc_D_7, 0, mix_osc_7, 3);
AudioConnection          patchCord19(osc_A_7, 0, mix_osc_7, 0);
AudioConnection          patchCord20(osc_C_7, 0, mix_osc_7, 2);
AudioConnection          patchCord21(osc_D_5, 0, mix_osc_5, 3);
AudioConnection          patchCord22(osc_A_5, 0, mix_osc_5, 0);
AudioConnection          patchCord23(osc_B_5, 0, mix_osc_5, 1);
AudioConnection          patchCord24(osc_D_2, 0, mix_osc_2, 3);
AudioConnection          patchCord25(osc_B_7, 0, mix_osc_7, 1);
AudioConnection          patchCord26(osc_C_3, 0, mix_osc_3, 2);
AudioConnection          patchCord27(osc_B_6, 0, mix_osc_6, 1);
AudioConnection          patchCord28(osc_C_6, 0, mix_osc_6, 2);
AudioConnection          patchCord29(osc_B_3, 0, mix_osc_3, 1);
AudioConnection          patchCord30(osc_A_6, 0, mix_osc_6, 0);
AudioConnection          patchCord31(osc_A_3, 0, mix_osc_3, 0);
AudioConnection          patchCord32(osc_D_6, 0, mix_osc_6, 3);
AudioConnection          patchCord33(mix_osc_5, 0, filter5, 0);
AudioConnection          patchCord34(mix_osc_5, 0, mix_filter_5, 3);
AudioConnection          patchCord35(mix_osc_7, 0, filter7, 0);
AudioConnection          patchCord36(mix_osc_7, 0, mix_filter_7, 3);
AudioConnection          patchCord37(mix_osc_8, 0, filter8, 0);
AudioConnection          patchCord38(mix_osc_8, 0, mix_filter_8, 3);
AudioConnection          patchCord39(mix_osc_6, 0, filter6, 0);
AudioConnection          patchCord40(mix_osc_6, 0, mix_filter_6, 3);
AudioConnection          patchCord41(mix_osc_1, 0, filter1, 0);
AudioConnection          patchCord42(mix_osc_1, 0, mix_filter_1, 3);
AudioConnection          patchCord43(mix_osc_2, 0, filter2, 0);
AudioConnection          patchCord44(mix_osc_2, 0, mix_filter_2, 3);
AudioConnection          patchCord45(mix_osc_4, 0, filter4, 0);
AudioConnection          patchCord46(mix_osc_4, 0, mix_filter_4, 3);
AudioConnection          patchCord47(mix_osc_3, 0, filter3, 0);
AudioConnection          patchCord48(mix_osc_3, 0, mix_filter_3, 3);
AudioConnection          patchCord49(filter6, 0, mix_filter_6, 0);
AudioConnection          patchCord50(filter6, 1, mix_filter_6, 1);
AudioConnection          patchCord51(filter6, 2, mix_filter_6, 2);
AudioConnection          patchCord52(filter8, 0, mix_filter_8, 0);
AudioConnection          patchCord53(filter8, 1, mix_filter_8, 1);
AudioConnection          patchCord54(filter8, 2, mix_filter_8, 2);
AudioConnection          patchCord55(filter5, 0, mix_filter_5, 0);
AudioConnection          patchCord56(filter5, 1, mix_filter_5, 1);
AudioConnection          patchCord57(filter5, 2, mix_filter_5, 2);
AudioConnection          patchCord58(filter7, 0, mix_filter_7, 0);
AudioConnection          patchCord59(filter7, 1, mix_filter_7, 1);
AudioConnection          patchCord60(filter7, 2, mix_filter_7, 2);
AudioConnection          patchCord61(filter1, 0, mix_filter_1, 0);
AudioConnection          patchCord62(filter1, 1, mix_filter_1, 1);
AudioConnection          patchCord63(filter1, 2, mix_filter_1, 2);
AudioConnection          patchCord64(filter4, 0, mix_filter_4, 0);
AudioConnection          patchCord65(filter4, 1, mix_filter_4, 1);
AudioConnection          patchCord66(filter4, 2, mix_filter_4, 2);
AudioConnection          patchCord67(filter2, 0, mix_filter_2, 0);
AudioConnection          patchCord68(filter2, 1, mix_filter_2, 1);
AudioConnection          patchCord69(filter2, 2, mix_filter_2, 2);
AudioConnection          patchCord70(filter3, 0, mix_filter_3, 0);
AudioConnection          patchCord71(filter3, 1, mix_filter_3, 1);
AudioConnection          patchCord72(filter3, 2, mix_filter_3, 2);
AudioConnection          patchCord73(mix_filter_5, env5);
AudioConnection          patchCord74(mix_filter_6, env6);
AudioConnection          patchCord75(mix_filter_1, env1);
AudioConnection          patchCord76(mix_filter_2, env2);
AudioConnection          patchCord77(mix_filter_8, env8);
AudioConnection          patchCord78(mix_filter_7, env7);
AudioConnection          patchCord79(mix_filter_3, env3);
AudioConnection          patchCord80(mix_filter_4, env4);
AudioConnection          patchCord81(env1, 0, mix_in_A, 0);
AudioConnection          patchCord82(env3, 0, mix_in_A, 2);
AudioConnection          patchCord83(env4, 0, mix_in_A, 3);
AudioConnection          patchCord84(env2, 0, mix_in_A, 1);
AudioConnection          patchCord85(env8, 0, mix_in_B, 3);
AudioConnection          patchCord86(env7, 0, mix_in_B, 2);
AudioConnection          patchCord87(env6, 0, mix_in_B, 1);
AudioConnection          patchCord88(env5, 0, mix_in_B, 0);
AudioConnection          patchCord89(mix_in_B, 0, mix_out, 1);
AudioConnection          patchCord90(mix_in_A, 0, mix_out, 0);
AudioConnection          patchCord91(mix_out, 0, i2s1, 0);
AudioConnection          patchCord92(mix_out, 0, i2s1, 1);
AudioControlSGTL5000     codec;     //xy=69,21
// GUItool: end automatically generated code




// Las estructuras

// Esto no es personalizable, Hay que declarar más objetos AudioSynthWaveform para hacer uso de más voces
const uint8_t NUM_VOCES = 8;
const uint8_t NUM_DETUNE_OCTAVES = 5;

struct tCanal

{
   uint8_t   waveform = 0;
   float A;   // Envelope Atttack
   float D;   // Envelope Decay
   float S;   // Envelope Sustain
   float R;   // Envelope Release
   float V;   // Volumen
   float detune = 0;
   int   detune_octave = 0;
   float bendFactor = 1;
   int   bendRange = 12;
   int   filterMode = 0;
   
};

struct tVoz
{
  uint8_t nota = 0;
  uint32_t t0  = 0;
};

tCanal canal[16];
tVoz   voz[8];

int detune_octaves[5]={-24,-12,0,12,24};

AudioSynthWaveform  *oscs[NUM_VOCES] = {
  &osc_A_1, &osc_A_2, &osc_A_3, &osc_A_4, 
  &osc_A_5, &osc_A_6, &osc_A_7, &osc_A_8
};

AudioSynthWaveform  *oscs_sec[NUM_VOCES] = {
  &osc_B_1, &osc_B_2, &osc_B_3, &osc_B_4, 
  &osc_B_5, &osc_B_6, &osc_B_7, &osc_B_8
};

AudioEffectEnvelope *envs[NUM_VOCES] = {
  &env1, &env2, &env3, &env4,
  &env5, &env6, &env7, &env8
};

AudioMixer4 *voiceMixers[NUM_VOCES] = {
  &mix_osc_1, &mix_osc_2, &mix_osc_3, &mix_osc_4,
  &mix_osc_5, &mix_osc_6, &mix_osc_7, &mix_osc_8
};

AudioFilterStateVariable *filters[NUM_VOCES] = {
  &filter1, &filter2, &filter3, &filter4,
  &filter5, &filter6, &filter7, &filter8 
};

AudioMixer4 *filterMixers[NUM_VOCES] = {
  &mix_filter_1, &mix_filter_2, &mix_filter_3, &mix_filter_4,
  &mix_filter_5, &mix_filter_6, &mix_filter_7, &mix_filter_8
};

const uint8_t NUM_WHAVESHAPES = 4;

uint8_t waveType[NUM_WHAVESHAPES] = {
  WAVEFORM_SINE,
  WAVEFORM_SQUARE,
  WAVEFORM_TRIANGLE,
  WAVEFORM_SAWTOOTH
};

void printWaveshape(uint8_t w) {
  switch (w) {
    case 0: Serial.print("sine"); break;
    case 1: Serial.print("square"); break;
    case 2: Serial.print("triangle"); break;
    case 3: Serial.print("saw"); break;
  }
}


void setup() {
  delay(1500);
  Serial.println("uZyn reloaded");

  myusb.begin();
  
  midi1.setHandleNoteOn(myNoteOn);
  midi1.setHandleNoteOff(myNoteOff);
  midi1.setHandleControlChange(myControlChange);
  midi1.setHandlePitchChange(myPitchChange);

  usbMIDI.setHandleNoteOff(myNoteOff);
  usbMIDI.setHandleNoteOn(myNoteOn);
  usbMIDI.setHandleControlChange(myControlChange);
  usbMIDI.setHandlePitchChange(myPitchChange);

  AudioMemory(18);
  
  codec.enable();
  codec.volume(0.45);

  for (uint8_t i=0; i<NUM_VOCES; i++) {
    for (uint8_t mixChannel=0; mixChannel<4; mixChannel++) {
      voiceMixers[i]->gain(mixChannel,0.25);
    }
  }
}

void loop() {
  myusb.Task();
  midi1.read();
  usbMIDI.read();

}

void myNoteOn(byte channel, byte note, byte velocity) {
  uint8_t daVoice = elegirVoz();
  voz[daVoice].nota = note;
  voz[daVoice].t0 = millis();
  tocarNota(channel,note,daVoice,velocity);
  printVoces();
}

void myNoteOff(byte channel, byte note, byte velocity) {
  Serial.print("Note Off, ch=");
  Serial.print(channel, DEC);
  Serial.print(", note=");
  Serial.print(note, DEC);
  Serial.print(", velocity=");
  Serial.println(velocity, DEC);

  uint8_t daVoice = buscarVozNota(note);
  if (daVoice != NUM_VOCES+1) {
    voz[daVoice].nota=0;
    pararNota(channel,note,daVoice);
  }
  
  printVoces();
}

void myControlChange(byte channel, byte control, byte value) {

  //canal[channel]

  switch (control) {
    case CC_ATTACK:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" Attack ");
      canal[channel].A=mapf(value,0,127,ATTACK_TIME_MIN,ATTACK_TIME_MAX);
      Serial.println(canal[channel].A);
      for (uint8_t  i=0; i <NUM_VOCES; i++) {
        envs[i]->attack(canal[channel].A);
      }
      break;
      
    case CC_DECAY:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" Decay ");
      canal[channel].D=mapf(value,0,127,DECAY_TIME_MIN,DECAY_TIME_MAX);
      Serial.println(canal[channel].D);
      for (uint8_t  i=0; i <NUM_VOCES; i++) {
        envs[i]->decay(canal[channel].D);
      }
      break;

    case CC_SUSTAIN:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" Sustain ");
      canal[channel].S=mapf(value,0,127,SUSTAIN_LEV_MIN,SUSTAIN_LEV_MAX);
      Serial.println(canal[channel].S);
      for (uint8_t  i=0; i <NUM_VOCES; i++) {
        envs[i]->sustain(canal[channel].S);
      }
      break;

    case CC_RELEASE:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" Release ");
      canal[channel].R=mapf(value,0,127,RELEASE_TIME_MIN,RELEASE_TIME_MAX);
      Serial.println(canal[channel].R);
      for (uint8_t  i=0; i <NUM_VOCES; i++) {
        envs[i]->release(canal[channel].R);
      }
      break;

    case CC_VOLUMEN:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" Volumen ");
      canal[channel].V=mapf(value,0,127,VOLUMEN_MIN,VOLUMEN_MAX);
      Serial.println(canal[channel].V);
      break;

    case CC_WAVEFORM:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" W ");
      if (value >=64) {
        canal[channel].waveform =  (canal[channel].waveform +1)%NUM_WHAVESHAPES ;
      }
      Serial.print("Nueva forma de onda "); printWaveshape(canal[channel].waveform); Serial.println();
      break;

    case CC_DETUNE_OCTAVE:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" Detune octave ");
      
      if (value >=0 && value < 26) {
        canal[channel].detune_octave= -24;
      }  
      else {
        if (value >=26 && value < 51) {
          canal[channel].detune_octave= -12 ;
        }
        else {
          if (value >=51 && value < 76) {
            canal[channel].detune_octave= 0;
          }
          else { 
            if (value >=76 && value < 102) {
              canal[channel].detune_octave= 12;
            }
            else {
              canal[channel].detune_octave= 24;       
            }
          }         
       }
     }
   
      Serial.print("Detune octave "); Serial.println(detune_octaves[canal[channel].detune_octave]);
      break;

    case CC_DETUNE_LEVEL:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" Detune factor ");
        canal[channel].detune = 1 - 0.05 * mapf(value,0,127,0,1);
      Serial.print("Detune level "); Serial.println(canal[channel].detune);
      cambiarVoces(channel);
      break;

     case CC_FILTER_FREQ:
      Serial.print("Canal "); Serial.print(channel); Serial.println(" Filter freq ");
      for (uint8_t i=0; i<NUM_VOCES; i++) {
        filters[i]->frequency(10000 * (value * 1.0/127.0));
      }     
      break;

    case CC_FILTER_MODE:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" ");
      if (value>=64) {
        selectFilter(channel);
      }
      break;

    case CC_FILTER_RES:
      Serial.print("Canal "); Serial.print(channel); Serial.print(" filter resonance ");
      for (uint8_t i=0; i<NUM_VOCES; i++) {
        filters[i]->resonance((4.3 * (value * 1/127)) + 0.7);
      }
      Serial.println((4.3 * (value * 1/127)) + 0.7);
      break;

      
    default:
      Serial.print("Control Change, ch=");
      Serial.print(channel, DEC);
      Serial.print(", control=");
      Serial.print(control, DEC);
      Serial.print(", value=");
      Serial.println(value, DEC);
      break;
  }
}

void myPitchChange(byte channel, int pitch) {
  float bendF = pitch;
  bendF = bendF / 8192;
  bendF = bendF * canal[channel].bendRange;
  bendF = bendF / 12;
  canal[channel].bendFactor = pow(2, bendF);
  cambiarVoces(channel);
}

void selectFilter(uint8_t ch) {
  canal[ch].filterMode = (canal[ch].filterMode +1) % 4;
  switch (canal[ch].filterMode) {
    case 0:
      for (uint8_t v = 0; v<NUM_VOCES; v++) {
        filterMixers[v]->gain(0,1);
        filterMixers[v]->gain(1,0);
        filterMixers[v]->gain(2,0);
        filterMixers[v]->gain(3,0);
      }
      Serial.println("Low pass filter selected");
      break;
    case 1:
      for (uint8_t v = 0; v<NUM_VOCES; v++) {
        filterMixers[v]->gain(0,0);
        filterMixers[v]->gain(1,1);
        filterMixers[v]->gain(2,0);
        filterMixers[v]->gain(3,0);
      }
      Serial.println("Band pass filter selected");
      break;
    case 2:
      for (uint8_t v = 0; v<NUM_VOCES; v++) {
        filterMixers[v]->gain(0,0);
        filterMixers[v]->gain(1,0);
        filterMixers[v]->gain(2,1);
        filterMixers[v]->gain(3,0);      
      }
      Serial.println("Hi pass filter selected");
      break;
    case 3:
      for (uint8_t v = 0; v<NUM_VOCES; v++) {
        filterMixers[v]->gain(0,0);
        filterMixers[v]->gain(1,0);
        filterMixers[v]->gain(2,0);
        filterMixers[v]->gain(3,1);
      }
      Serial.println("Hi pass filter selected");
    break;
  }
}

